# -*- coding: utf-8 -*-

import cookielib, json, os, re, time
import urllib2
import urllib
import requests
from urlparse import urlparse
from HTMLParser import HTMLParser

class FormParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.url = None
        self.params = {}
        self.in_form = False
        self.form_parsed = False
        self.method = "GET"

    def handle_starttag(self, tag, attrs):
        tag = tag.lower()
        if tag == "form":
            if self.form_parsed:
                raise RuntimeError("Second form on page")
            if self.in_form:
                raise RuntimeError("Already in form")
            self.in_form = True 
        if not self.in_form:
            return
        attrs = dict((name.lower(), value) for name, value in attrs)
        if tag == "form":
            self.url = attrs["action"] 
            if "method" in attrs:
                self.method = attrs["method"]
        elif tag == "input" and "type" in attrs and "name" in attrs:
            if attrs["type"] in ["hidden", "text", "password"]:
                self.params[attrs["name"]] = attrs["value"] if "value" in attrs else ""

    def handle_endtag(self, tag):
        tag = tag.lower()
        if tag == "form":
            if not self.in_form:
                raise RuntimeError("Unexpected end of <form>")
            self.in_form = False
            self.form_parsed = True


class VKApi():

    obj = None

    def __init__(self, email, password, client_id):

        #vktoken = "/var/run/tagbot/vktoken"
        #if os.path.exists(vktoken):
        #    tmp = json.loads(file(vktoken, 'r').read().strip())
        #    self.token = tmp[0]
        #    self.user_id = tmp[1]
        #else:
        self.token, self.user_id = self.auth(email, password, client_id, "audio,wall,photos")
        #tmp = [self.token, self.user_id]
        #file(vktoken,'w+').write("%s\n" % json.dumps(tmp))


    def call_api(self, method, params):

        if isinstance(params, list):
            params_list = [kv for kv in params]
        elif isinstance(params, dict):
            params_list = params.items()
        else:
            params_list = [params]
        params_list.append(("access_token", self.token))
        url = "https://api.vk.com/method/%s?%s" % (method, urllib.urlencode(params_list))
        data = urllib2.urlopen(url).read()
        return json.loads(data)["response"]

    def auth_user(self, email, password, client_id, scope, opener):
        response = opener.open(
            "http://oauth.vk.com/oauth/authorize?" + \
            "redirect_uri=http://oauth.vk.com/blank.html&response_type=token&" + \
            "client_id=%s&scope=%s&display=wap" % (client_id, ",".join(scope))
            )
        doc = response.read()
        parser = FormParser()
        parser.feed(doc)
        parser.close()
        if not parser.form_parsed or parser.url is None or "pass" not in parser.params or \
          "email" not in parser.params:
              raise RuntimeError("Something wrong")
        parser.params["email"] = email
        parser.params["pass"] = password
        if parser.method == "post":
            response = opener.open(parser.url, urllib.urlencode(parser.params))
        else:
            print parser.method
            raise NotImplementedError("fail")
        return response.read(), response.geturl()

    def give_access(self, doc, opener):
        parser = FormParser()
        parser.feed(doc)
        parser.close()
        if not parser.form_parsed or parser.url is None:
              raise RuntimeError("Something wrong")
        if parser.method.lower() == "post":
            response = opener.open(parser.url, urllib.urlencode(parser.params))
        else:
            raise NotImplementedError("Method '%s'" % parser.method)
        return response.geturl()


    def auth(self, email, password, client_id, scope):
        if not isinstance(scope, list):
            scope = [scope]
        opener = urllib2.build_opener(
            urllib2.HTTPCookieProcessor(cookielib.CookieJar()),
            urllib2.HTTPRedirectHandler())
        doc, url = self.auth_user(email, password, client_id, scope, opener)
        if urlparse(url).path != "/blank.html":
            # Need to give access to requested scope
            url = self.give_access(doc, opener)
        if urlparse(url).path != "/blank.html":
            raise RuntimeError("Expected success here")
    
        def split_key_value(kv_pair):
            kv = kv_pair.split("=")
            return kv[0], kv[1]
    
        answer = dict(split_key_value(kv_pair) for kv_pair in urlparse(url).fragment.split("&"))
        if "access_token" not in answer or "user_id" not in answer:
            raise RuntimeError("Missing some values in answer")
        return answer["access_token"], answer["user_id"] 


    def distance(self, source, dest):
        try:
            source = source.encode("utf-8")            
        except UnicodeDecodeError:
            source = source
        try:
            dest = dest.encode("utf-8")
        except UnicodeDecodeError:
            dest = dest
        if source == dest:
            return 0
    
        # Prepare matrix
        slen, dlen = len(source), len(dest)
        dist = [[0 for i in range(dlen+1)] for x in range(slen+1)]
        for i in xrange(slen+1):
            dist[i][0] = i
        for j in xrange(dlen+1):
            dist[0][j] = j
    
        # Counting distance
        for i in xrange(slen):
            for j in xrange(dlen):
                cost = 0 if source[i] == dest[j] else 1
                dist[i+1][j+1] = min(
                                dist[i][j+1] + 1,
                                dist[i+1][j] + 1,
                                dist[i][j] + cost
                            )
        return dist[-1][-1]


    # Постим на стенку
    def WallPost(self, message, attachments):

        attachments = ",".join(attachments)

        f = self.call_api("wall.post", [("owner_id", "-47694403"), ("message", message), ("attachments", attachments), ("from_group",1), ("signed",0)]) 


    # Постим файл
    def FileUpload(self, filed, gid):

        # Узнаём серве, на который заливать картинку
        f = self.call_api("photos.getWallUploadServer", [("gid", gid)])

        # Заливаем
        files = {'photo': open(filed, 'rb')}
        r = requests.post(f['upload_url'], files=files)
        response = json.loads(r.text)

        # Постим
        f = self.call_api("photos.saveWallPhoto", [("server", response['server']), ("photo", response['photo']), ("hash", response['hash']), ("gid", gid)])

        return f[0]['id']

    # Ищем музыку
    def musicSearch(self, name):
        f = self.call_api("audio.search", [("q", name.encode("utf-8")), ("auto_complete", 1), ("count", 100)]) 
        b = []
        for j in f[1:]:
            result = "%s - %s" % (j['artist'].encode("utf-8"), j['title'].encode("utf-8"))
            distance = self.distance(re.sub("The\s+", '', name), re.sub("The\s+", '', result))
            b.append([result, distance, j['url'], j['artist'].encode("utf-8"), j['title'].encode("utf-8"), j['duration'], j['aid'], j['owner_id']])
        c = b
        c.sort(key = lambda b:b[1])
        try:
            ret = None
            for cc in c:
                try:
                    size = int(os.popen("wget --spider --server-response '%s' 2>&1 | grep 'Content-Length:' | awk '{print $2}'" % cc[2]).read())
                    duration = cc[5]
                    bitrate = size * 8 / duration / 1024
                except:
                    bitrate = 0

                if bitrate > 170:
                    ret = cc
                    break
                else:
                    time.sleep(1)

            if ret == None:
                ret = c[0]

        except IndexError:
            return False, False, f, False, False
        return ret[2], ret[3], ret[4], ret[6], ret[7]
