# -*- coding: utf-8 -*-

import json
import os


# Класс, для работы с плейлистом


class Playlist():


    def __init__(self, filename = ""):

        # Если плейлиста не существуем, бросаем ошибку
        if not os.path.exists(filename):
            raise Exception("Playlist '%s' not found!")

        self.fileName = filename

        # Выгружаем плейлист в память
        import shutil
        shutil.copy2(filename, '%s.bak' % filename)

        obj = open(filename, "r")
        self.playList = json.loads(obj.read())
        obj.close()



    # Вернуть справочную информацию о плейлисте    
    def Info(self):

        return {
            "tracks": len(self.playList["tracks"]),
            "lib": len(self.playList["lib"]),
            "errors": len(self.playList["errors"])
        }


    # Вернуть первый трек из плейлиста, удалив его
    def Pop(self):

        # Чтобы взять первый элемент, список не должен быть пустым
        if len(self.playList["tracks"]) < 1:
            raise Exception("Плейлист пуст!")

        #track = self.playList["tracks"].pop(0)
        track = self.playList["tracks"][0]
        track["Artist"] = track["Artist"].encode("utf8")
        track["Name"] = track["Name"].encode("utf8")
        return track


    # Добавить упешный трек в библиотеку
    def AddToLib(self, song):

        self.playList["lib"].append(song)


    # Удалить первый трек из плейлиста
    def DeleteFirst(self):

        self.playList["tracks"].pop(0)


    # Добавить трек в список ошибочных
    def Error(self, song):

        self.playList["errors"].append(song)


    # Применить изменения и сохранить плейлист
    def isSimilarInLibrary(self, song):

        cnt = len([ s for s in self.playList["lib"] 
            if s['Artist'] == song['Artist'] 
            and s['Name'] == song['Name'] 
            and s['Album'] == song['Album'] ])
        return cnt > 0


    # Применить изменения и сохранить плейлист
    def Save(self):

        # Конвертируем в JSON и сохраняем
        obj = open(self.fileName, "w")
        obj.write(json.dumps(self.playList, indent=4))
        obj.close()